let
  pkgs = import nix/nixpkgs.nix {};
  hsPkgs = import nix/package-set.nix { inherit pkgs; };
in hsPkgs.shellFor {
  packages = ps: [ ps.sop-static ];
  withHoogle = false;
  exactDeps = true;

  nativeBuildInputs = [
    pkgs.haskellPackages.cabal-install
    hsPkgs.hlint.components.exes.hlint
    hsPkgs.stylish-haskell.components.exes.stylish-haskell
    hsPkgs.haskell-language-server.components.exes.haskell-language-server
  ];
}
