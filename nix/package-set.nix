{ pkgs ? import ./nixpkgs.nix {} }:

with import ./utils { inherit pkgs; };

mkStackagePkgSet {
  resolver = "lts-17.3";
  fromSrc.sop-static = {
    cabalFile = ../sop-static.cabal;
    src = cleanGitHaskellSource { src = ../.; };
  };
  fromHackage = (import ./utils/hackage-sets.nix).hls_1_1_0_0 // {
    stylish-haskell = "0.12.2.0";
  };
  modules = [
    {
      packages.stylish-haskell.patches = pkgs.callPackage ./utils/stylish-haskell-patches.nix {};
    }
  ];
}
