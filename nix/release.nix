with import ./nixpkgs.nix {};

let
  hsPkgs = import ./package-set.nix { inherit pkgs; };
  utils = import ./utils.nix { inherit pkgs; };
in hsPkgs.sop-static.checks // rec {
  inherit (hsPkgs.sop-static.components) library;
  inherit (library) doc;
}
