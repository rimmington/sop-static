{...}@args:

# Blep

let
  src = builtins.fetchTarball {
    url = "https://github.com/input-output-hk/haskell.nix/archive/006e792f1c48508d0e9a57598d3f81388a5c86ec.tar.gz";
    sha256 = "18svfj0a9m7ykjp02fi0qcf3q188l8bdkdnlminnzk9vpzz51c23";
  };
  haskell-nix = import src args;
in import haskell-nix.sources.nixpkgs (args // {
  overlays = args.overlays or [] ++
    haskell-nix.overlays ++
    [
      (self: super: { haskell-nix-src = src; })
    ];
})
