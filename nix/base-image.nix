let
  pkgs = import ./nixpkgs.nix {};
  hsPkgs = import ./package-set.nix { inherit pkgs; };
  utils = import ./utils { inherit pkgs; };
in utils.baseImage {
  name = "base-image";
  compilerNixName = hsPkgs._config.compiler.nix-name;
  extraPaths = [
    { name = "haskell-nix-src"; path = pkgs.haskell-nix-src; }
  ];
}
