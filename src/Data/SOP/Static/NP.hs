{-# language GADTs #-}

module Data.SOP.Static.NP
  ( ccollapseNP, cpureNP, ctraverse'NP, czipWithNP
  , cpureZipNP, ctraverseZip'NP
  , P, cpureZip1NP, ctraverseZip1'NP
  , cfromList
  ) where

import Data.SOP (K (..), NP (..), unK)
import Data.SOP.Static.Constraint (All (..), AllZip (..))
import GHC.Exts (Proxy#, proxy#)
import RIO

cpureNP :: forall c xs f. (All c xs) => Proxy# c -> (forall a. c a => f a) -> NP f xs
cpureNP p f = cparaAll p Nil cons
  where
    cons :: forall y ys. c y => NP f ys -> NP f (y : ys)
    cons = (f @y :*)
    {-# INLINE cons #-}
{-# INLINABLE cpureNP #-}

newtype Traversal f f' g xs = Traversal { doTraverse :: NP f xs -> g (NP f' xs) }

ctraverse'NP ::
    forall c xs f f' g.
    (All c xs, Applicative g)
 => Proxy# c
 -> (forall a. c a => f a -> g (f' a))
 -> NP f xs
 -> g (NP f' xs)
ctraverse'NP p f = doTraverse $ cparaAll p nil cons
  where
    nil = Traversal $ \_ -> pure Nil
    {-# INLINE nil #-}
    cons :: forall y ys. (c y) => Traversal f f' g ys -> Traversal f f' g (y : ys)
    cons (Traversal g) = Traversal $ \(x :* xs) -> (:*) <$> f x <*> g xs
    {-# INLINE cons #-}
{-# INLINABLE ctraverse'NP #-}

newtype PureZip f xs ys zs = PureZip { doPureZip :: NP f zs }

cpureZipNP ::
    forall c xs ys f p zs.
    (AllZip p c xs ys zs)
 => Proxy# p
 -> Proxy# c
 -> Proxy# xs
 -> Proxy# ys
 -> (forall a b. c a b => f (p a b))
 -> NP f zs
cpureZipNP p c _ _ f = doPureZip $ cparaAllZip @_ @_ @xs @ys p c (PureZip Nil) cons
  where
    cons ::
        forall a as b bs ps.
        (c a b)
     => PureZip f as bs ps
     -> PureZip f (a : as) (b : bs) (p a b : ps)
    cons (PureZip np) = PureZip $ f @a @b :* np
    {-# INLINE cons #-}
{-# INLINABLE cpureZipNP #-}

newtype PureZip1 f xs ys zs = PureZip1 { doPureZip1 :: NP f xs }

cpureZip1NP ::
    forall c xs ys f zs.
    (AllZip P c xs ys zs)
 => Proxy# c
 -> Proxy# xs
 -> Proxy# ys
 -> (forall a b. c a b => Proxy# b -> f a)
 -> NP f xs
cpureZip1NP c _ _ f = doPureZip1 $ cparaAllZip @P @_ @xs @ys proxy# c (PureZip1 Nil) cons
  where
    cons ::
        forall a as b bs ps.
        (c a b)
     => PureZip1 f as bs ps
     -> PureZip1 f (a : as) (b : bs) (P a b : ps)
    cons (PureZip1 np) = PureZip1 $ f @a @b proxy# :* np
    {-# INLINE cons #-}
{-# INLINABLE cpureZip1NP #-}

newtype TraversalZip f g h r xs ys zs = TraversalZip { doTraverseZip :: NP f xs -> NP g ys -> r (NP h zs) }

ctraverseZip'NP ::
    forall c xs ys f g h r p zs.
    (AllZip p c xs ys zs, Applicative r)
 => Proxy# p
 -> Proxy# c
 -> (forall a b. c a b => f a -> g b -> r (h (p a b)))
 -> NP f xs
 -> NP g ys
 -> r (NP h zs)
ctraverseZip'NP p c f = doTraverseZip $ cparaAllZip @_ @c @xs @ys p c nil cons
  where
    nil = TraversalZip $ \_ _ -> pure Nil
    {-# INLINE nil #-}
    cons ::
        forall a as b bs ps.
        (c a b)
     => TraversalZip f g h r as bs ps
     -> TraversalZip f g h r (a : as) (b : bs) (p a b : ps)
    cons (TraversalZip g) = TraversalZip $ \(a :* as) (b :* bs) -> (:*) <$> f a b <*> g as bs
    {-# INLINE cons #-}
{-# INLINABLE ctraverseZip'NP #-}

newtype TraversalZip1 f f' g xs ys zs = TraversalZip1 { doTraverseZip1 :: NP f xs -> g (NP f' xs) }

data P a b

ctraverseZip1'NP ::
    forall c xs ys f f' g zs.
    (AllZip P c xs ys zs, Applicative g)
 => Proxy# c
 -> Proxy# ys
 -> (forall a b. c a b => Proxy# b -> f a -> g (f' a))
 -> NP f xs
 -> g (NP f' xs)
ctraverseZip1'NP c _ f = doTraverseZip1 $ cparaAllZip @_ @c @xs @ys (proxy# @P) c nil cons
  where
    nil = TraversalZip1 $ \_ -> pure Nil
    {-# INLINE nil #-}
    cons ::
        forall a as b bs ps.
        (c a b)
     => TraversalZip1 f f' g as bs ps
     -> TraversalZip1 f f' g (a : as) (b : bs) (P a b : ps)
    cons (TraversalZip1 g) = TraversalZip1 $ \(x :* xs) -> (:*) <$> f (proxy# @b) x <*> g xs
    {-# INLINE cons #-}
{-# INLINABLE ctraverseZip1'NP #-}

newtype Consume b f (xs :: [k]) = Consume { doConsume :: NP f xs -> [b] }

ccollapseNP ::
    forall c xs f b. All c xs
 => Proxy# c
 -> (forall a. c a => f a -> K b a)
 -> NP f xs
 -> [b]
ccollapseNP p f = doConsume $
    cparaAll p (Consume $ const []) cons
  where
    cons :: forall y ys. (c y) => Consume b f ys -> Consume b f (y : ys)
    cons (Consume g) = Consume $ \(x :* xs) -> unK (f x) : g xs
    {-# INLINE cons #-}
{-# INLINABLE ccollapseNP #-}

newtype ZipConsume f g h xs = ZipConsume { doZipConsume :: NP f xs -> NP g xs -> NP h xs }

czipWithNP ::
    forall c xs f g h.
    All c xs
 => Proxy# c
 -> (forall a. c a => f a -> g a -> h a)
 -> NP f xs
 -> NP g xs
 -> NP h xs
czipWithNP p f = doZipConsume $ cparaAll p (ZipConsume $ \Nil Nil -> Nil) cons
  where
    cons :: forall y ys. (c y) => ZipConsume f g h ys -> ZipConsume f g h (y : ys)
    cons (ZipConsume g) = ZipConsume $ \(fx :* fxs) (gx :* gxs) ->
        f fx gx :* g fxs gxs
    {-# INLINE cons #-}
{-# INLINABLE czipWithNP #-}

newtype Emusonc a xs = Emusonc { doEmusonc :: [a] -> Maybe (NP (K a) xs) }

cfromList :: forall c a xs. (All c xs) => Proxy# c -> [a] -> Maybe (NP (K a) xs)
cfromList p = doEmusonc $ cparaAll p nil cons
  where
    nil :: Emusonc a '[]
    nil = Emusonc $ \case
        [] -> Just Nil
        _  -> Nothing
    {-# INLINE nil #-}
    cons :: forall y ys. Emusonc a ys -> Emusonc a (y : ys)
    cons (Emusonc f) = Emusonc $ \case
        (a : as) -> (K a :*) <$> f as
        []       -> Nothing
