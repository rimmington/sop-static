{-# language GADTs, UndecidableInstances #-}

module Data.SOP.Static.Constraint
  ( All (..), AllZip (..), mapAllDict
  ) where

import Data.SOP.Dict (Dict (..))
import GHC.Exts (Proxy#)

class All c xs where
    cparaAll :: Proxy# c -> r '[] -> (forall y ys. (c y, All c ys) => r ys -> r (y : ys)) -> r xs

instance All c '[] where
    cparaAll = \_ nil _ -> nil
    {-# INLINE cparaAll #-}

instance (c x, All c xs) => All c (x : xs) where
    cparaAll = \p nil cons -> cons (cparaAll p nil cons)
    {-# INLINE cparaAll #-}

class AllZip p c xs ys zs | p xs ys -> zs where
    cparaAllZip ::
        Proxy# p
     -> Proxy# c
     -> r '[] '[] '[]
     -> (forall a as b bs ps. c a b => r as bs ps -> r (a : as) (b : bs) (p a b : ps))
     -> r xs ys zs

instance (zs ~ '[], ys ~ '[]) => AllZip p c '[] ys zs where
    cparaAllZip = \_ _ nil _ -> nil
    {-# INLINE cparaAllZip #-}

instance (c x y, ys ~ (y : yt), zs ~ (p x y : zt), AllZip p c xs yt zt) =>
        AllZip p c (x : xs) ys zs where
    cparaAllZip = \p c nil cons -> cons (cparaAllZip p c nil cons)
    {-# INLINE cparaAllZip #-}

mapAllDict ::
    forall c d xs.
    All c xs
 => Proxy# c
 -> (forall a. c a => Dict d a)
 -> Dict (All d) xs
mapAllDict p f = cparaAll p Dict cons
  where
    cons :: forall y ys. (c y) => Dict (All d) ys -> Dict (All d) (y : ys)
    cons Dict = case f @y of
        Dict -> Dict
    {-# INLINE cons #-}
{-# INLINABLE mapAllDict #-}
