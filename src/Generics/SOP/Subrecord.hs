{-# language TypeFamilies, UndecidableInstances, UndecidableSuperClasses #-}

module Generics.SOP.Subrecord
  ( IsSubrecordOf (..)
  , HasField (..), HasProductTypeInfo
  ) where

import Data.Kind (Type)
import Data.SOP (I (I))
import Data.SOP.Static.Constraint (AllZip)
import Data.SOP.Static.NP (P, cpureZip1NP)
import GHC.Exts (Proxy#, proxy#)
import GHC.Records qualified as R
import Generics.SOP (DatatypeInfoOf, HasDatatypeInfo, IsProductType, productTypeTo)
import Generics.SOP.Type.Metadata qualified as M
import RIO

class HasField (a :: Type) (t :: Type) (fi :: M.FieldInfo) where
    getField :: Proxy# fi -> a -> t
instance (fi ~ 'M.FieldInfo n, R.HasField n a t) => HasField a t fi where
    getField _ = R.getField @n

class (HasDatatypeInfo a, DatatypeInfoOf a ~ i) =>
    HasProductTypeInfo a i (ci :: [M.ConstructorInfo]) | a i -> ci
instance (HasDatatypeInfo a, DatatypeInfoOf a ~ M.ADT mn dn cis sis, ci ~ cis) =>
    HasProductTypeInfo a (M.ADT mn dn cis sis) ci
instance (HasDatatypeInfo a, DatatypeInfoOf a ~ M.Newtype mn dn nc, ci ~ '[nc]) =>
    HasProductTypeInfo a (M.Newtype mn dn nc) ci

class IsSubrecordOf a b where
    subrecord :: a -> b
instance
    ( IsProductType b ts
    , HasProductTypeInfo b di '[M.Record cn fis]
    , AllZip P (HasField a) ts fis zs)
 => IsSubrecordOf a b where
    subrecord a = productTypeTo $ cpureZip1NP @(HasField a) @ts @fis proxy# proxy# proxy# f
      where
        f :: forall t fi. (HasField a t fi) => Proxy# fi -> I t
        f p = I $ getField p a
