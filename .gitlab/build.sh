#!/usr/bin/env nix-shell
#!nix-shell -i bash -p ponysay -p gnused -p coreutils

set -euo pipefail

tmp=$(mktemp -d)

attempt () (
    # Determine which paths are not available in binary caches
    nix-build --dry-run nix/release.nix 2>&1 \
        | tee >(cat 1>&2) \
        | sed -e '0,/will be built/d' -e '/will be fetched/,$d' \
        > $tmp/to-be-built

    # Build everything we can
    res=0
    nix-store --realise --keep-going $(cat $tmp/to-be-built) || res=$?

    # Cache everything that needed building
    if [ -n "${CACHE_DEST:-}" ]; then
        echo Uploading to cache...
        for drv in $(cat $tmp/to-be-built); do
            declare -a outputs
            outputs=($(nix-store -q --outputs "$drv"))
            for output in "${outputs[@]}"; do
                if [ -e $output ]; then
                    nix copy --to "${CACHE_DEST}&secret-key=$CACHE_SECRET" $output
                fi
            done
        done
    fi

    exit $res
)

res=0
# bash is stupid https://unix.stackexchange.com/a/254676
attempt &
wait $! || res=$?

if [ $res -eq 0 ]; then
  2>/dev/null ponysay --pony pinkie -b ascii -- "It worked!"
else
  2>/dev/null ponysay --pony pinkamena -b ascii -- "It broke..."
fi

exit $res
